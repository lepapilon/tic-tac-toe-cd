function Controller() {
    function nextPlayer() {
        G.currentPlayer = (G.currentPlayer + 1) % 2;
        return G.players[G.currentPlayer];
    }
    function doclick() {
        "" == this.title ? this.title = nextPlayer() : alert("Invalid move!");
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.index = Ti.UI.createWindow({
        backgroundColor: "white",
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    $.__views.btn1 = Ti.UI.createButton({
        width: "20%",
        height: "20%",
        borderWidth: 4,
        borderColor: "#AAA",
        backgroundColor: "#FFF",
        color: "#000",
        top: "10%",
        left: "10%",
        id: "btn1"
    });
    $.__views.index.add($.__views.btn1);
    doclick ? $.__views.btn1.addEventListener("click", doclick) : __defers["$.__views.btn1!click!doclick"] = true;
    $.__views.btn2 = Ti.UI.createButton({
        width: "20%",
        height: "20%",
        borderWidth: 4,
        borderColor: "#AAA",
        backgroundColor: "#FFF",
        color: "#000",
        top: "10%",
        left: "40%",
        id: "btn2"
    });
    $.__views.index.add($.__views.btn2);
    doclick ? $.__views.btn2.addEventListener("click", doclick) : __defers["$.__views.btn2!click!doclick"] = true;
    $.__views.btn3 = Ti.UI.createButton({
        width: "20%",
        height: "20%",
        borderWidth: 4,
        borderColor: "#AAA",
        backgroundColor: "#FFF",
        color: "#000",
        top: "10%",
        left: "70%",
        id: "btn3"
    });
    $.__views.index.add($.__views.btn3);
    doclick ? $.__views.btn3.addEventListener("click", doclick) : __defers["$.__views.btn3!click!doclick"] = true;
    $.__views.btn4 = Ti.UI.createButton({
        width: "20%",
        height: "20%",
        borderWidth: 4,
        borderColor: "#AAA",
        backgroundColor: "#FFF",
        color: "#000",
        top: "40%",
        left: "10%",
        id: "btn4"
    });
    $.__views.index.add($.__views.btn4);
    doclick ? $.__views.btn4.addEventListener("click", doclick) : __defers["$.__views.btn4!click!doclick"] = true;
    $.__views.btn5 = Ti.UI.createButton({
        width: "20%",
        height: "20%",
        borderWidth: 4,
        borderColor: "#AAA",
        backgroundColor: "#FFF",
        color: "#000",
        top: "40%",
        left: "40%",
        id: "btn5"
    });
    $.__views.index.add($.__views.btn5);
    doclick ? $.__views.btn5.addEventListener("click", doclick) : __defers["$.__views.btn5!click!doclick"] = true;
    $.__views.btn6 = Ti.UI.createButton({
        width: "20%",
        height: "20%",
        borderWidth: 4,
        borderColor: "#AAA",
        backgroundColor: "#FFF",
        color: "#000",
        top: "40%",
        left: "70%",
        id: "btn6"
    });
    $.__views.index.add($.__views.btn6);
    doclick ? $.__views.btn6.addEventListener("click", doclick) : __defers["$.__views.btn6!click!doclick"] = true;
    $.__views.btn7 = Ti.UI.createButton({
        width: "20%",
        height: "20%",
        borderWidth: 4,
        borderColor: "#AAA",
        backgroundColor: "#FFF",
        color: "#000",
        top: "70%",
        left: "10%",
        id: "btn7"
    });
    $.__views.index.add($.__views.btn7);
    doclick ? $.__views.btn7.addEventListener("click", doclick) : __defers["$.__views.btn7!click!doclick"] = true;
    $.__views.btn8 = Ti.UI.createButton({
        width: "20%",
        height: "20%",
        borderWidth: 4,
        borderColor: "#AAA",
        backgroundColor: "#FFF",
        color: "#000",
        top: "70%",
        left: "40%",
        id: "btn8"
    });
    $.__views.index.add($.__views.btn8);
    doclick ? $.__views.btn8.addEventListener("click", doclick) : __defers["$.__views.btn8!click!doclick"] = true;
    $.__views.btn9 = Ti.UI.createButton({
        width: "20%",
        height: "20%",
        borderWidth: 4,
        borderColor: "#AAA",
        backgroundColor: "#FFF",
        color: "#000",
        top: "70%",
        left: "70%",
        id: "btn9"
    });
    $.__views.index.add($.__views.btn9);
    doclick ? $.__views.btn9.addEventListener("click", doclick) : __defers["$.__views.btn9!click!doclick"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    var G = {
        currentPlayer: 1,
        players: [ "X", "O" ]
    };
    $.index.open();
    __defers["$.__views.btn1!click!doclick"] && $.__views.btn1.addEventListener("click", doclick);
    __defers["$.__views.btn2!click!doclick"] && $.__views.btn2.addEventListener("click", doclick);
    __defers["$.__views.btn3!click!doclick"] && $.__views.btn3.addEventListener("click", doclick);
    __defers["$.__views.btn4!click!doclick"] && $.__views.btn4.addEventListener("click", doclick);
    __defers["$.__views.btn5!click!doclick"] && $.__views.btn5.addEventListener("click", doclick);
    __defers["$.__views.btn6!click!doclick"] && $.__views.btn6.addEventListener("click", doclick);
    __defers["$.__views.btn7!click!doclick"] && $.__views.btn7.addEventListener("click", doclick);
    __defers["$.__views.btn8!click!doclick"] && $.__views.btn8.addEventListener("click", doclick);
    __defers["$.__views.btn9!click!doclick"] && $.__views.btn9.addEventListener("click", doclick);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;